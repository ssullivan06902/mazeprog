defmodule Maze.Algorithm do
  def binary_tree(w, h) do
    grid = Grid.new(w, h)
    rows = Grid.rows(grid) |> Enum.reverse
    
    bt(Grid.new(w, h), rows)
  end

  defp bt(grid, []) do
    grid
  end

  defp bt(grid, [h|t]) do
    bt(bt_row(grid, h), t)
  end

  defp bt_row(grid, []) do
    grid
  end

  defp bt_row(grid, [%{coords: coords}|t]) do
    cell = grid.cells[coords]
    neighbors = Enum.filter([cell.north, cell.east], &(not is_nil(&1)))
    index = trunc(:rand.uniform * length(neighbors))
    neighbor = grid.cells[Enum.at(neighbors, index)]

    if neighbor do
      bt_row(Cell.link(grid, cell, neighbor), t)
    else
      bt_row(grid, t)
    end
  end
end
