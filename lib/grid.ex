defmodule Grid do
  defstruct [:width, :height, :cells]

  def new(w, h) do
    cells = for x <- 0..w-1,
                y <- 0..h-1,
                into: %{},
                do: {{x, y}, %Cell{coords: {x, y}, links: %MapSet{}}}
    
    grid = %Grid{width: w, height: h, cells: cells}

    configure_cells(grid)
  end

  defp configure_cells(%Grid{width: w, height: h} = grid) do
    neighbor? = fn (coords) ->
      if grid.cells[coords], do: coords, else: nil
    end
      
    cells = for x <- 0..w-1,
                y <- 0..h-1,
                into: %{},
                do: {{x, y},
	             %Cell{coords: {x, y},
			   links:  %MapSet{},
			   north:  neighbor?.({x, y-1}),
			   east:   neighbor?.({x+1, y}),
			   south:  neighbor?.({x, y+1}),
			   west:   neighbor?.({x-1, y})}
		     }

    %{grid | cells: cells}
  end

  def rows(grid) do
    w = grid.width-1
    h = grid.height-1

    for y <- 0..h do
      for x <- 0..w do
	grid.cells[{x, y}]
      end
    end
  end
end
